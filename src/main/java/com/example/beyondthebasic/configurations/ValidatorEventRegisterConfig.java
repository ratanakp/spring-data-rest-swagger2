package com.example.beyondthebasic.configurations;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Configuration
public class ValidatorEventRegisterConfig implements InitializingBean {


    private ValidatingRepositoryEventListener validatingRepositoryEventListener;
    private Map<String, Validator> validatorMap;

    public ValidatorEventRegisterConfig(ValidatingRepositoryEventListener validatingRepositoryEventListener, Map<String, Validator> validatorMap) {
        this.validatingRepositoryEventListener = validatingRepositoryEventListener;
        this.validatorMap = validatorMap;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        List<String> events = Arrays.asList("beforeSave");

        for (Map.Entry<String, Validator> entry :
                validatorMap.entrySet()) {

            events.stream()
                    .filter(p -> entry.getKey().startsWith(p))
                    .findFirst()
                    .ifPresent(
                            p -> validatingRepositoryEventListener
                                    .addValidator(p, entry.getValue())
                    );

        }

    }
}
