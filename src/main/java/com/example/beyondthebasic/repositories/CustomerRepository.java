package com.example.beyondthebasic.repositories;

import com.example.beyondthebasic.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@RepositoryRestResource
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    @RestResource(path = "/meme")
    List<Customer> findAllByName(@PathVariable("name") String name);

}
