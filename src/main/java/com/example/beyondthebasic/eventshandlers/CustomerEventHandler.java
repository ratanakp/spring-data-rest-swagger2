package com.example.beyondthebasic.eventshandlers;

import com.example.beyondthebasic.entities.Customer;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

@RepositoryEventHandler(Customer.class)
public class CustomerEventHandler {

//    Logger logger = LogManager.getLogger();

    @HandleBeforeCreate
    public void handleCustomerBeforeCreate(Customer customer) {
//        logger.info("meme --> @HandleBeforeCreate: " + customer.getName());

    }




}
