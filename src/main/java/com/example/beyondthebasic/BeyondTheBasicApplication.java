package com.example.beyondthebasic;

import com.example.beyondthebasic.entities.Customer;
import com.example.beyondthebasic.repositories.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;


@SpringBootApplication
@EnableSwagger2
public class BeyondTheBasicApplication implements ApplicationRunner {

    private CustomerRepository customerRepository;

    public BeyondTheBasicApplication(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(BeyondTheBasicApplication.class, args);
    }



    @Override
    public void run(ApplicationArguments args) throws Exception {
        customerRepository.save(new Customer("meme", "0909909990"));

        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("kfslfjsfsfskសដសថសថsdsdsdsl", "099034333"));
        customers.add(new Customer("Ok", "012332433"));
        customers.add(new Customer("Good", "01063354353"));

//        customerRepository.saveAll(customers);
        customerRepository.saveAll(customers);
    }
}

