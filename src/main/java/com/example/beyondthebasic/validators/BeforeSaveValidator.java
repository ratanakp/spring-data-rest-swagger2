package com.example.beyondthebasic.validators;

import com.example.beyondthebasic.entities.Customer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class BeforeSaveValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {

        return Customer.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty");
        Customer p = (Customer) o;
        if (p.getName().length() > 10) {
            errors.rejectValue("name", ">10");
        }
    }

}
