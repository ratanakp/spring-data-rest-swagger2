package com.example.beyondthebasic.entities;

public class Meme {

    int id;

    public Meme() {

    }

    public Meme(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
